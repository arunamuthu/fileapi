const express = require('express');
const router = express.Router();
const fileController = require('../controller/fileController');
 
router.post('/upload',fileController.uploadFile);
router.get('/view',fileController.getFile);
router.delete('/delete/:file',fileController.deleteFile);
 
module.exports=router;