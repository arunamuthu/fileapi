const config= {
    env: process.env.NODE_ENV || 'development',
    server: {
        port: process.env.PORT || 4000,
    },
    google: {
        projectId: 'gcp-node-288106',
        bucket: 'gcp-file',
    },
};
 
module.exports={config}