const { Storage } = require('@google-cloud/storage');
const { config } = require('../config/config');
const util = require('util');

const { format } = util;

const path = require('path')
const serviceKey = path.join(__dirname, '../config/keys.json')

const storage = new Storage({
    projectId: config.google.projectId,
    keyFilename: serviceKey,
});

const bucket = storage.bucket(config.google.bucket);



module.exports = {
    uploadFile: async (request, response, next) => {
        try {
            const { originalname, buffer } = request.file;
            const blob = bucket.file(originalname.replace(/ /g, "_"))
            const stream = blob.createWriteStream({
                resumable: true,
                predefinedAcl: 'publicRead',
            });
            stream.on('error', err => {
                console.log(err)
                next(err);
            });
            stream.on('finish', () => {
                const publicUrl = format(`https://storage.googleapis.com/${bucket.name}/${blob.name}`)
                response.status(200).json({
                    data: {
                        url: publicUrl,
                    },
                });
            });
            stream.end(request.file.buffer);
        }
        catch (error) {
            console.log(error);
            next(error)
        }
    },

    deleteFile: async (request, response, next) => {
        const filename = request.params.file;
        await bucket.file(filename).delete();
        response.status(200).json({
            data: {
                message: `${filename} is deleted`,
            },
        });

    },

    getFile: async (request, response, next) => {
        const [files] = await bucket.getFiles();
        var result = [];

        files.forEach((file) => {
            result.push({ "name": file.name, "size": file.metadata.size, "date": file.metadata.timeCreated });
        });
        response.send(result);
    }
}

