const express = require('express')
 
const bodyParser = require('body-parser')
const app = express()
const FileRouter=require('./routes/router');
 
const multer = require('multer')
const cors = require(`cors`)
app.all('*', function(req, res, next) {
  var origin = req.get('origin'); 
  res.header('Access-Control-Allow-Origin', origin);
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  next();
});
app.use(cors());

const multerMid = multer({
  storage: multer.memoryStorage(),
  limits: {
    fileSize: 5 * 1024 * 1024,
  },
});

app.disable('x-powered-by')
 
app.use(multerMid.single('file'))
 
app.use(bodyParser.json())
 
app.use(bodyParser.urlencoded({extended: false}))
 
app.use('/',FileRouter);

app.get('/',(req,res)=>{
  res.send("backend worked")
})
 
app.use((err, req, res, next) => {
  res.status(500).json({
    error: err,
    message: 'Internal server error!',
  })
  next()
})
 
app.listen(9001, () => {
  console.log('app now listening for requests!!!')
})